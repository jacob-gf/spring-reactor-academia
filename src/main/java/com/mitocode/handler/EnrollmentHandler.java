package com.mitocode.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.mitocode.model.Enrollment;
import com.mitocode.service.IEnrollmentService;
import com.mitocode.validators.RequestValidator;

import reactor.core.publisher.Mono;

import java.net.URI;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

@Component
public class EnrollmentHandler {

	@Autowired
	private IEnrollmentService service;
	
	@Autowired
	private RequestValidator validadorGeneral;
		
	public Mono<ServerResponse> findAll(ServerRequest req){
		return ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(service.findAll(), Enrollment.class);
	}
	public Mono<ServerResponse> findById(ServerRequest req){
		String id = req.pathVariable("id");
		
		return service.findById(id)
				.flatMap(p -> ServerResponse.ok()
							.contentType(MediaType.APPLICATION_JSON)
							.body(fromValue(p))
				)
				.switchIfEmpty(ServerResponse
						.notFound()
						.build()
				);
					
	}
	
	public Mono<ServerResponse> create(ServerRequest req){
		Mono<Enrollment> monoMatricula = req.bodyToMono(Enrollment.class);

		return monoMatricula
				.flatMap(this.validadorGeneral::validar)
				.flatMap(service::save)
				.flatMap(p -> ServerResponse.created(URI.create(req.uri().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(fromValue(p))
				);
		
	}
	
	public Mono<ServerResponse> update(ServerRequest req){
		Mono<Enrollment> monoMatricula = req.bodyToMono(Enrollment.class);
		String id = req.pathVariable("id");

		return service.findById(id)
				.zipWith(monoMatricula, (monoDb, monoReq) -> {
					monoDb.setStatus(monoReq.isStatus());
					monoDb.setStudent(monoReq.getStudent());
					monoDb.setCourses(monoReq.getCourses());
					monoDb.setDate(monoReq.getDate());
					return monoDb;
				})
				.flatMap(this.validadorGeneral::validar)
				.flatMap(service::update)
				.flatMap(p -> ServerResponse.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(fromValue(p))
				).switchIfEmpty(ServerResponse.notFound().build());
	}
	
	public Mono<ServerResponse> delete(ServerRequest req){
		String id = req.pathVariable("id");
		
		return service.findById(id)
				.flatMap(p -> service.delete(p.getId())
							.then(ServerResponse
									.noContent()
									.build()
							)						
				)
				.switchIfEmpty(ServerResponse
						.notFound()
						.build());		
	}
}
