package com.mitocode.handler;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

import java.net.URI;

import com.mitocode.model.Course;
import com.mitocode.service.ICourseService;
import com.mitocode.validators.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;


import reactor.core.publisher.Mono;

@Component
public class CourseHandler {

	@Autowired
	private ICourseService service;
	
	@Autowired
	private RequestValidator validadorGeneral;
		
	public Mono<ServerResponse> findAll(ServerRequest req){
		return ServerResponse.ok()
				.contentType(MediaType.APPLICATION_STREAM_JSON)
				.body(service.findAll(), Course.class);
	}
	
	public Mono<ServerResponse> findById(ServerRequest req){
		String id = req.pathVariable("id");
		
		return service.findById(id)
				.flatMap(p -> ServerResponse.ok()
							.contentType(MediaType.APPLICATION_STREAM_JSON)
							.body(fromValue(p))
				)
				.switchIfEmpty(ServerResponse
						.notFound()
						.build()
				);
					
	}
	
	public Mono<ServerResponse> create(ServerRequest req){
		Mono<Course> platoMono = req.bodyToMono(Course.class);
		
		return platoMono
				.flatMap(this.validadorGeneral::validar)		
				.flatMap(service::save)
			.flatMap(p -> ServerResponse.created(URI.create(req.uri().toString().concat("/").concat(p.getId())))
			.contentType(MediaType.APPLICATION_STREAM_JSON)
			.body(fromValue(p))
		);
		
	}
	
	public Mono<ServerResponse> update(ServerRequest req){
		Mono<Course> platoMono = req.bodyToMono(Course.class);
		
		return platoMono
				.flatMap(this.validadorGeneral::validar)		
				.flatMap(service::update)
			.flatMap(p -> ServerResponse.ok()
			.contentType(MediaType.APPLICATION_STREAM_JSON)
			.body(fromValue(p))
		).switchIfEmpty(ServerResponse
				.notFound()
				.build()
		);					
	}
	
	public Mono<ServerResponse> delete(ServerRequest req){
		String id = req.pathVariable("id");
		
		return service.findById(id)
				.flatMap(p -> service.delete(p.getId())
							.then(ServerResponse
									.noContent()
									.build()
							)						
				)
				.switchIfEmpty(ServerResponse
						.notFound()
						.build());		
	}
}
