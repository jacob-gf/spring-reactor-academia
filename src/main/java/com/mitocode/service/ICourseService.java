package com.mitocode.service;

import org.springframework.data.domain.Pageable;

import com.mitocode.model.Course;
import com.mitocode.pagination.PageSupport;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICourseService extends ICRUD<Course, String> {

	Flux<Course> listarDemorado();
	Flux<Course> listarSobrecargado();
}
