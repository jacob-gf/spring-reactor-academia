package com.mitocode.service.impl;

import java.time.Duration;
import com.mitocode.repository.IGenericRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import com.mitocode.model.Course;
import com.mitocode.repository.ICourseRepository;
import com.mitocode.service.ICourseService;

import reactor.core.publisher.Flux;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl extends CRUDImpl<Course, String> implements ICourseService {


	private final ICourseRepository repository;



	@Override
	protected IGenericRepo<Course, String> getRepo() {
		return repository;
	}

	@Override
	public Flux<Course> listarDemorado() {
		return repository.findAll().delayElements(Duration.ofSeconds(1));
	}

	@Override
	public Flux<Course> listarSobrecargado() {
		return repository.findAll().repeat(3000);
	}

}
