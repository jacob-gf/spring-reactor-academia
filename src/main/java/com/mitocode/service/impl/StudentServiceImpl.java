package com.mitocode.service.impl;

import java.time.Duration;

import com.mitocode.repository.IGenericRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.mitocode.model.Student;
import com.mitocode.repository.IStudentRepository;
import com.mitocode.service.IStudentService;

import reactor.core.publisher.Flux;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl extends CRUDImpl<Student, String>  implements IStudentService {


	private final IStudentRepository repository;

	@Override
	protected IGenericRepo<Student, String> getRepo() {
		return repository;
	}


	@Override
	public Flux<Student> findAllByOrderDesc() {
		// TODO Auto-generated method stub
		return repository.findAll(Sort.by(Sort.Direction.DESC, "age"));
	}
	@Override
	public Flux<Student> findAllByOrderAsc() {
		// TODO Auto-generated method stub
		return repository.findAll(Sort.by(Sort.Direction.ASC, "age"));
	}

}
