package com.mitocode.service.impl;

import com.mitocode.repository.IGenericRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import com.mitocode.model.Enrollment;
import com.mitocode.repository.IEnrollmentRepository;
import com.mitocode.service.IEnrollmentService;


@Service
@RequiredArgsConstructor
public class EnrollmentServiceImpl extends CRUDImpl<Enrollment, String> implements IEnrollmentService {

	private final IEnrollmentRepository repository;

	@Override
	protected IGenericRepo<Enrollment, String> getRepo() {
		return repository;
	}
}
