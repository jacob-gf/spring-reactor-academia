package com.mitocode.service;

import org.springframework.data.domain.Pageable;

import com.mitocode.model.Enrollment;
import com.mitocode.pagination.PageSupport;

import reactor.core.publisher.Mono;

public interface IEnrollmentService extends ICRUD<Enrollment, String> {

}
