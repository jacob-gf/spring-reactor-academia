package com.mitocode.service;

import org.springframework.data.domain.Pageable;

import com.mitocode.model.Student;
import com.mitocode.pagination.PageSupport;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IStudentService extends ICRUD<Student, String> {

	Flux<Student> findAllByOrderDesc();
	Flux<Student> findAllByOrderAsc();
}
