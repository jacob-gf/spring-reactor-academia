package com.mitocode.config;

import com.mitocode.handler.CourseHandler;
import com.mitocode.handler.StudentHandler;
import com.mitocode.handler.EnrollmentHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.PUT;
import static org.springframework.web.reactive.function.server.RequestPredicates.DELETE;

@Configuration
public class RouterConfig {

    //Functional Endpoints
    @Bean
    public RouterFunction<ServerResponse> routesCourse(CourseHandler handler){
        return route(GET("/v2/courses"), handler::findAll) //req -> handler.findAll(req)
                .andRoute(GET("/v2/courses/{id}"), handler::findById)
                .andRoute(POST("/v2/courses"), handler::create)
                .andRoute(PUT("/v2/courses/{id}"), handler::update)
                .andRoute(DELETE("/v2/courses/{id}"), handler::delete);
    }

    @Bean
    public RouterFunction<ServerResponse> routesStudent(StudentHandler handler){
        return route(GET("/v2/students"), handler::findAll) //req -> handler.findAll(req)
                .andRoute(GET("/v2/students/{id}"), handler::findById)
                .andRoute(POST("/v2/students"), handler::create)
                .andRoute(PUT("/v2/students/{id}"), handler::update)
                .andRoute(DELETE("/v2/students/{id}"), handler::delete);
    }

    @Bean
    public RouterFunction<ServerResponse> routesEnrollment(EnrollmentHandler handler){
        return route(GET("/v2/enrollments"), handler::findAll) //req -> handler.findAll(req)
                .andRoute(GET("/v2/enrollments/{id}"), handler::findById)
                .andRoute(POST("/v2/enrollments"), handler::create)
                .andRoute(PUT("/v2/enrollments/{id}"), handler::update)
                .andRoute(DELETE("/v2/enrollments/{id}"), handler::delete);
    }
}
