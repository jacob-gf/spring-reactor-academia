package com.mitocode.repository;

import com.mitocode.model.Course;


public interface ICourseRepository extends IGenericRepo<Course, String> {

}
	