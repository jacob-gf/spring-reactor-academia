package com.mitocode.controller;

import java.net.URI;


import com.mitocode.model.Enrollment;
import com.mitocode.service.IEnrollmentService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/enrollments")
@RequiredArgsConstructor
public class EnrollmentController {

	private final IEnrollmentService service;
	
	@GetMapping
	public Mono<ResponseEntity<Flux<Enrollment>>> listar(){
		Flux<Enrollment> cursosFlux = service.findAll();
		
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(cursosFlux));
	}
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<Enrollment>> listarPorId(@PathVariable("id") String id){
		return service.findById(id)	//Mono.empty()
				//.defaultIfEmpty(new Matricula())
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)
				).defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@PostMapping
	public Mono<ResponseEntity<Enrollment>> registrar(@Valid @RequestBody Enrollment enrollment, final ServerHttpRequest req){
		return service.save(enrollment)
				.map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)
					).onErrorMap(error -> new ArithmeticException("Ups"));
	}

	@PutMapping("/{id}")
	public Mono<ResponseEntity<Enrollment>> actualizar(@Valid @RequestBody Enrollment request, @PathVariable("id") String id){
		return this.service.findById(id)
				.flatMap(matriculaDB -> {
					matriculaDB.setCourses(request.getCourses());
					matriculaDB.setStatus(request.isStatus());
					matriculaDB.setStudent(request.getStudent());
					matriculaDB.setDate(request.getDate());
					return this.service.update(matriculaDB);
				}).flatMap(matriculaUpdated -> Mono.just(ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(matriculaUpdated)))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id){
		return service.findById(id)
				.flatMap(p -> {
					return service.delete(p.getId())
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
				})
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
}
