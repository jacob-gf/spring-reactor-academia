package com.mitocode.controller;

import java.net.URI;


import com.mitocode.model.Student;
import com.mitocode.service.IStudentService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

	private final IStudentService service;
	
	@GetMapping
	public Mono<ResponseEntity<Flux<Student>>> listar(){
		Flux<Student> estudiantesFlux = service.findAll();
		
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(estudiantesFlux));
	}
		
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<Student>> listarPorId(@PathVariable("id") String id){
		return service.findById(id)	//Mono.empty()
				//.defaultIfEmpty(new Cliente())
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)
				).defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@PostMapping
	public Mono<ResponseEntity<Student>> registrar(@Valid @RequestBody Student student, final ServerHttpRequest req){
		return service.save(student)
				.map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)
					).onErrorMap(error -> new ArithmeticException("Ups"));
	}

	@PutMapping("/{id}")
	public Mono<ResponseEntity<Student>> actualizar(@Valid @RequestBody Student estudiante, @PathVariable("id") String id){
		return this.service.findById(id)
				.flatMap(estudianteDB -> {
					estudianteDB.setDni(estudiante.getDni());
					estudianteDB.setName(estudiante.getName());
					estudianteDB.setLastName(estudiante.getLastName());
					estudianteDB.setAge(estudiante.getAge());
					return this.service.update(estudianteDB);
				}).flatMap(estudianteUpdated -> Mono.just(ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(estudianteUpdated)))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id){
		return service.findById(id)
				.flatMap(p -> {
					return service.delete(p.getId())
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
				})
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@GetMapping("/age-desc")
	public Mono<ResponseEntity<Flux<Student>>> findAllByOrderDesc(){
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(this.service.findAllByOrderDesc())
		).defaultIfEmpty(ResponseEntity.notFound().build());
	}
	@GetMapping("/age-asc")
	public Mono<ResponseEntity<Flux<Student>>> findAllByOrderAsc(){
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(this.service.findAllByOrderAsc())
		).defaultIfEmpty(ResponseEntity.notFound().build());
	}


}
