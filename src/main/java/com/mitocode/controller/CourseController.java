package com.mitocode.controller;

import java.net.URI;

import jakarta.validation.Valid;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Course;
import com.mitocode.service.ICourseService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CourseController {

	private final ICourseService service;
	
	@GetMapping
	public Mono<ResponseEntity<Flux<Course>>> listar(){
		
						
		Flux<Course> platosFlux = service.findAll();
		
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(platosFlux));
	}
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<Course>> listarPorId(@PathVariable("id") String id){
		return service.findById(id)	//Mono.empty()
				//.defaultIfEmpty(new Cliente())
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)
				).defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@PostMapping
	public Mono<ResponseEntity<Course>> registrar(@Valid @RequestBody Course course, final ServerHttpRequest req){
		return service.save(course)
				.map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)
					).onErrorMap(error -> new ArithmeticException("Ups"));
	}

	@PutMapping("/{id}")
	public Mono<ResponseEntity<Course>> actualizar(@Valid @RequestBody Course curso, @PathVariable("id") String id){
		return this.service.findById(id)
				.flatMap(cursoDB -> {
					cursoDB.setStatus(curso.isStatus());
					cursoDB.setName(curso.getName());
					cursoDB.setAcronyms(curso.getAcronyms());
					return this.service.update(cursoDB);
				}).flatMap(cursoUpdated -> Mono.just(ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(cursoUpdated)))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id){
		return service.findById(id)
				.flatMap(p -> {
					return service.delete(p.getId())
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
				})
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	

}
