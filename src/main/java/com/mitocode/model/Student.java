package com.mitocode.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Document(collection = "students")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Student {

	@Id
	private String id;

	@NotEmpty
	@Size(min = 3)
	private String name;

	@NotEmpty
	@Size(min = 3)
	private String lastName;

	@NotEmpty
	@Size(min = 3)
	private String dni;
	

	private int age;

}
